import { getCurrencySymbol } from '@angular/common';
import { Component, Injectable,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PageService } from 'src/app/service/page.service';
import { Flight } from './flight';
import {
  NgbDateStruct,
  NgbCalendar,
  NgbDatepickerI18n,
  NgbCalendarBuddhist,
  NgbDate,
  NgbDateParserFormatter
} from '@ng-bootstrap/ng-bootstrap';
import localeThai from '@angular/common/locales/th';
import { getLocaleDayNames, FormStyle, TranslationWidth, getLocaleMonthNames, formatDate, registerLocaleData } from '@angular/common';

@Injectable()
export class NgbDatepickerI18nBuddhist extends NgbDatepickerI18n {

  private _locale = 'th';
  private _weekdaysShort: readonly string[];
  private _monthsShort: readonly string[];
  private _monthsFull: readonly string[];

  constructor() {
    super();

    registerLocaleData(localeThai);

    const weekdaysStartingOnSunday = getLocaleDayNames(this._locale, FormStyle.Standalone, TranslationWidth.Short);
    this._weekdaysShort = weekdaysStartingOnSunday.map((day, index) => weekdaysStartingOnSunday[(index + 1) % 7]);

    this._monthsShort = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Abbreviated);
    this._monthsFull = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Wide);
  }

  getMonthShortName(month: number): string { return this._monthsShort[month - 1] || ''; }

  getMonthFullName(month: number): string { return this._monthsFull[month - 1] || ''; }

  getWeekdayLabel(weekday: number) {
    return this._weekdaysShort[weekday - 1] || '';
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    const jsDate = new Date(date.year, date.month - 1, date.day);
    return formatDate(jsDate, 'fullDate', this._locale);
  }

  override getYearNumerals(year: number): string { return String(year); }
}


@Component({
  selector: 'app-bookflight',
  templateUrl: './bookflight.component.html',
  styleUrls: ['./bookflight.component.css'],
  providers: [
    { provide: NgbCalendar, useClass: NgbCalendarBuddhist },
    { provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBuddhist },
  ],
})
export class BookflightComponent implements OnInit {

  hoveredDate: NgbDate | null = null;
  serviceflights !: Flight[];
  flight : Flight;
  flightForm : FormGroup;
  selectControl:FormControl = new FormControl();

  constructor(private fb: FormBuilder,private pageService: PageService,public calendar: NgbCalendar, public formatter: NgbDateParserFormatter) {
    this.flight = new Flight("","","","",calendar.getToday(),calendar.getNext(calendar.getToday(), 'd', 3),1,0,0);
    this.flightForm = this.fb.group({
        fullname:['',Validators.required],
        from: ['',Validators.required],
        to: ['',Validators.required],
        type: ['',Validators.required],
        departure: [calendar.getToday()],
        arrival: [calendar.getNext(calendar.getToday(), 'd', 3)],
        adults: ['',[Validators.required,Validators.pattern("^[1-9]*$")]],
        children: ['',[Validators.required,Validators.pattern("^[0-9]*$")]],
        infants: ['',[Validators.required,Validators.pattern("^[0-9]*$")]],
    });
    this.getPage();
  }

  getPage(){
    this.serviceflights = this.pageService.getPages();
  }

  onSubmit(f: Flight): void {
    console.log(getCurrencySymbol);
    this.pageService.addFlight(f);
  }


  flights = [
    {value: 'กรุงเทพ-BKK'},
    {value: 'กรุงเทพ-DMK'},
    {value: 'เชียงใหม่-CNX'},
    {value: 'เชียงราย-CEI'},
    {value: 'ภูเก็ต-HKT'},
    {value: 'กระบี่-KBV'},
    {value: 'หาดใหญ่-HDY'},
    {value: 'เกาะสมุย-USM'},
    {value: 'พัทยา-UTP'},
  ];


  ngOnInit(): void {
  }

  onDateSelection(date: NgbDate) {
    if (!this.flightForm.value.departure && !this.flightForm.value.arrival) {
      this.flightForm.value.departure = date;
    } else if (this.flightForm.value.departure && !this.flightForm.value.arrival && date && date.after(this.flightForm.value.departure)) {
      this.flightForm.value.arrival = date;
    } else {
      this.flightForm.value.arrival = null;
      this.flightForm.value.departure = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.flightForm.value.departure && !this.flightForm.value.arrival && this.hoveredDate && date.after(this.flightForm.value.departure) &&
        date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) { return this.flightForm.value.arrival && date.after(this.flightForm.value.departure) && date.before(this.flightForm.value.arrival); }

  isRange(date: NgbDate) {
    return date.equals(this.flightForm.value.departure) || (this.flightForm.value.arrival && date.equals(this.flightForm.value.arrival)) || this.isInside(date) ||
        this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }




  changeDefaultName(
    fullName: string,
    from: string,
    to: string,
    type: string,
    adults: number,
    departure: NgbDate | null,
    children: number,
    infants: number,
    arrival: NgbDate | null
  ){
    this.flight.fullName = fullName;
    this.flight.from = from;
    this.flight.to = to;
    this.flight.type = type;
    this.flight.adults = adults;
    this.flight.departure = departure;
    this.flight.children = children;
    this.flight.infants = infants;
    this.flight.arrival = arrival;
  }


}




