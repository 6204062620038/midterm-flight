import { Flight } from "../component/bookflight/flight";
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
export class Mockflight {
  public static mFlights: Flight[] = [{
    fullName: "Wanatchaporn",
    from: "กรุงเทพ-DMK",
    to: "กระบี่-KBV",
    type: "Return",
    departure: NgbDate.from({year: 2022, month: 3, day: 5}),
    arrival: NgbDate.from({year: 2022, month: 3, day: 10}),
    adults: 1,
    children: 0,
    infants: 0,
  }
  ]
}
